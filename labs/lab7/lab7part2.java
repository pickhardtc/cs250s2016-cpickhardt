//Claire Pickhardt
//Computer Science 250
//Lab 7, Part 2
//1 April 2016
//Honor Code: The work that I am submitting is the result of my own thinking and efforts. Any help I received was from Prof. Wenskovitch or SJ.
//The goal of this section of the lab is to parse through a string and do a simple sort through the characters.

import java.util.*;
import java.io.*;

public class lab7part2{
    public static void main(String[] args) throws FileNotFoundException{
        String[] identifier;
        String[] sequence;
        int count=0;
        int maxcount=0;
        int similarityCount;
        char c;
        InputStream stream = null;
        Scanner scan;
        String line = "";
        ArrayList<String> id=null;
        ArrayList<String> seq=null;
        Integer[] countBlanks;


        try{

            scan = new Scanner(new File("cTIM_core_align.fa"));
            identifier = null;
            sequence = null;
            id = new ArrayList<String>();
            seq = new ArrayList<String>();
            while(scan.hasNextLine()){
                line = scan.nextLine().trim();
                if(line.length()!=0){
                if(line.charAt(0)=='>'){
                   // if(thing)
                       // thing = false;
                            id.add(line);
                    }
                    else{
                            seq.add(line);
                    }//else


            }}//while
        }//try
        catch(IOException E){
            System.out.println("Unsuccessful");}//catch

        System.out.println(seq.size());
        countBlanks = new Integer[seq.size()];
        int i,k;
        identifier = id.toArray(new String[id.size()]);
        sequence = seq.toArray(new String[seq.size()]);
        //This will count the max number of "-" and save it into a count variable.
        for(i=0; i<seq.size()-1; i++){
            c = '-';
            count = 0;
            line = seq.get(i);
            for(k=0; k<seq.get(i).length()-1; k++){
                if(line.charAt(k)==c){
                count++;
                }//if
            }//for
            System.out.println(count);
            countBlanks[i]=count;}
            System.out.println("Part One answers:");
                Sorter(countBlanks, identifier, sequence);

        int[] similarArray = new int[seq.size()];
        String seq1 = seq.get(seq.size()-1);
        for(i=0; i<(seq.size())-2; i++){
            String seqCheck = seq.get(i);
            int check = seqCheck.length();
            similarityCount = 0;

                for(int h = 0; h<check; h++){
                    if(Character.toString(seqCheck.charAt(h)).equals("-")){
                        similarityCount -= 1;
                    }//if
                    if(Character.toString(seqCheck.charAt(h)).equals(Character.toString(seq1.charAt(h)))){
                        similarityCount += 3;
                    }//if
                    else{
                        similarityCount -=2;
                    }//else
            }//nested for <`0`>
                similarArray[i] = similarityCount;
                System.out.println("Part two answers: "+similarArray[i]+identifier);
        }//for

    }//main

    public static void Sorter(Integer[] s, String[] identifier, String[] sequence){
        for(int i = 0; i<s.length-1; i++){
            System.out.println(s[0]);
            for(int j = i; j>0 && less(s[j], s[j-1]); j--){
                exchange(s, identifier, sequence, j, j-1);
            }//nested for <`0`>
        }//for
        int k=0;
        while(k<s.length-1){
            System.out.println(" "+k+" count = "+s[k]+" identifier = "+identifier[k]+" sequence = "+sequence[k]);
                k++;
        }//while
    }//sorter function

private static void exchange(Object[] s, String[] identifier, String[] sequence, int i, int j){
    Object hey = s[i];
    s[i] = s[j];
    s[j] = hey;

    String hello = identifier[i];
    identifier[i] = identifier[j];
    identifier[j] = hello;

    hello = sequence[i];
    sequence[i] = sequence[j];
    sequence[j] = hello;
}//exchange

 private static boolean less(Comparable v, Comparable w) {
        return (v.compareTo(w) < 0);
 }//less

}//lab7part2
