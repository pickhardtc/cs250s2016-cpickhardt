import java.util.*;
import java.lang.*;
import java.io.*;

class SortCompare {

    public static double time(String alg, String[] sequence) {
        Stopwatch timer = new Stopwatch();
        if (alg.equals("MSD")) {
            MSD.sort(sequence);
        } else if (alg.equals("LSD")) {
            LSD.sort(sequence, 240);
        } else if (alg.equals("Merge")) {
            Merge.sort(sequence);
        } else if (alg.equals("Quick")) {
            Quick.sort(sequence);
        }//if-else
        return timer.elapsedTime();
    } //time

    public static double timeRandomInput(String alg, String[] sequence) {
                double total = 0.0;
                int T = 5;
        //Double[] a = new Double[N];
        for (int t = 0; t < T; t++) {
            StdOut.printf("--Iteration %d\n", t);
            total += time(alg, sequence);
        } //for
        return total;
    } //timeRandomInput

    public static void main(String args[]){
        String[] identifier;
        String[] sequence;
        int count=0;
        int maxcount=0;
        char c;
        InputStream stream = null;
        Scanner scan;
        String line = "";
        ArrayList<String> id=null;
        ArrayList<String> seq=null;
        Integer[] countBlanks;
        try{
        scan = new Scanner(new File("cTIM_core_align.fa"));
            identifier = null;
            sequence = null;
            id = new ArrayList<String>();
            seq = new ArrayList<String>();
            while(scan.hasNextLine()){
                line = scan.nextLine().trim();
                if(line.length()!=0){
                if(line.charAt(0)=='>'){
                            id.add(line);
                    }
                    else{
                            seq.add(line);
                    }//else
                }//if

            }//while
        }//try
        catch(IOException E){
            System.out.println("Unsuccessful");
        }//catch

            sequence = seq.toArray(new String[seq.size()]);
            identifier = id.toArray(new String[id.size()]);
           // identifier = String.format("%-23s", identifier);
        if (args.length != 2) {
            StdOut.printf("USAGE: String alg1, String alg2, int array_size, int repeats");
        } else {
            String alg1 = args[0];
            String alg2 = args[1];
            double t1 = timeRandomInput(alg1, sequence);
            double t2 = timeRandomInput(alg2, sequence);
            StdOut.printf(" %s is", alg1);
            StdOut.printf(" %.3f times faster than %s\n", t2/t1, alg2);
        } //if-else
      } //main

} //SortCompare
