//Claire Pickhardt
//Lab 8
//Professor Wenskovitch
//Computer Science 250
//22 April 2016
//Honor Code: The work that I am submitting is the result of my own thinking and efforts.
//the reading in and output of the text file comes from http://www.java-tips.org/java-se-tips-100019/18-java-io/2028-how-to-read-file-in-java.html
//Almog and Izaak also helped me greatly when Prof was either helping other students or was not in his office.
//Izaak particularly helped me with the third and fourth queries. Props to him.
import java.util.*;
import java.io.*;

public class lab8{
    public static void main(String[] args){
        In file = new In(args[0]);
        File file = new File("airline.txt");
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        DataInputStream dis = null;
        Scanner scan = new Scanner(System.in);
        int query;
        float dollar;
        String city1;
        String city2;
        int v;
        ArrayList<String> list = new ArrayList<String>();
        EdgeWeightedGraph G = new EdgeWeightedGraph(file);
        ArrayList<String> cities = new ArrayList<String>();
        ArrayList<Integer> miles = new ArrayList<Integer>();
        ArrayList<Float> cost;

        try {
            fis = new FileInputStream(file);
            // Here BufferedInputStream is added for fast reading.
            bis = new BufferedInputStream(fis);
            dis = new DataInputStream(bis);
            Scanner s = new Scanner(new File("airline.txt"));
            v = Integer.parseInt(bis.read());
            for(int i =0; i<v; i++){
                cities[i] = bis.read();

            }//for
            while (s.hasNextLine()){
                list.add(s.nextLine());}
        }//try
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }//catch
        catch (IOException e) {
            e.printStackTrace();
        }//catch

        EdgeWeightedGraph graph = new EdgeWeightedGraph(cities);
        StringTokenizer st;
        int edge1, edge2;
        double dist;
        cost = new ArrayList<Integer>();
        for(int i = 0; i<miles.size(); i++){
            StringTokenizer str_token = new StringTokenizer(miles.get(i));
            edge1 = Integer.parseInt(str_token.nextToken());
            edge2 = Integer.parseInt(str_token.nextToken());
            dist = Double.parseDouble(str_token.nextToken());
            cost = Double.parseDouble(str_token.nextToken());
            Edge newEdge = new Edge(edge1-1, edge2-1, dist, cost);
            graph.addEdge(newEdge);
        }//for
        System.out.println("Enter the number that you would like to see: ");
        System.out.print(" 1) The entire list of direct distances, routes, and prices \n 2) A minimum spanning tree of the data \n 3) The shortest path search (has three subparts) \n 4) Pick a dollar amount and this will tell you where you can go \n 5) Add or remove a route from the schedule \n 6) Quit the program\n");
        query = scan.nextInt();

        if(query == 1){
            //Part 1 goes here
            try{
        // dis.available() returns 0 if the file does not have more lines.
      while (dis.available() != 0) {

      // this statement reads the line from the file and print it to
        // the console.
        System.out.println(dis.readLine());
      }}
    catch (FileNotFoundException e) {
      e.printStackTrace();
    }catch (IOException e) {
      e.printStackTrace();
    }


    }//1

    if(query == 2){
        //Part 2 goes here
       PrimMST mst = new PrimMST(G);
          for (Edge e : mst.edges()) {
            StdOut.println(e);
        }
    }//2

    if(query == 3){
        //Part 3 goes here
        System.out.println("Enter where you are!:");
        city1 = scan.nextLine();
        System.out.println("Where do you want to go?:");
        city2 = scan.nextLine();
        int tempv = 0;
        int goal = 0;
        for(int i = 0; i<D.V(); i++){
            if(G.getCity(i).equals(source)){
                tempv = i;
            }//if
        }//nested for

        for(int j = 0; j<D.V(); j++){
            if(G.getCity(j).equals(source)){
                tempv = j;
            }//if
        }//2nd nested for

        DijkstraSP sp = new DijkstraSP(D, tempv, 0);
        if(sp.hasPathTo(goal)){
            StdOut.pringf("%s to %s (%.2f miles) ", G.cities[tempv], G.cities[goal], sp.distTo(goal));

            if(sp.hasPathTo(goal)){
                for(DirectedEdge e: sp.pathTo(goal)){
                    StdOut.print(e + "  ");
                }//for
            }//if
        }//if
        else{
            StdOut.printf("%s to %s     There are no flights\n", G.cities[tempv], G.cities[goal]);
        }//else
        DijkstraSP SP = new DijkstraSP(F, tempv, 1);
        if (SP.hasPathTo(goal)) {
            StdOut.printf("%s to %s ($%.2f)  ", G.cities[tempv], G.cities[goal], SP.distTo(goal));
            if (SP.hasPathTo(goal)) {
                for (DirectedEdge e: SP.pathTo(goal)){
                    StdOut.print(e + "   ");
                }//for
            }//if
        }//if
        else {
            StdOut.printf("%s to %s      No Flight Available\n",G.cities[tempv], G.cities[tempv]);
        }//else
        BreadthFirstPaths bfs = new BreadthFirstPaths(H, tempv);
        if(bfs.hasPathTo(goal)){
            StdOut.printf("%s to %s (%d Flight(s)): ", G.cities[tempv], G.cities[goal], bfs.distTo(goal));
            for (int x: bfs.pathTo(goal)){
                if (x == tempv) StdOut.print(G.cities[x]);
                else              StdOut.print("-" + G.cities[x]);
            }//for
            StdOut.println();
        }//if

    }//3

    if(query == 4){
        //Part 4 goes here
        System.out.println("How much do you want to spend?");
        dollar = scan.nextFloat();

        for(int i = 0; i < D.V; i++){
            DepthFirstSearch DFS = new DepthFirstSearch(G, i, dollar);
            StdOut.printf("Your trip will cost less than or equal to %d", dollar);
            for(int j = 0; j < D.V(); j++){
                if(dfs.marked[j]){
                    StdOut.print(G.cities[j] + " ");
                }//if
            }//for

            if(dfs.count() != D.V()){
                StdOut.println("Graph is not connected.");
            }//if
            else{
                System.out.println("The graph is connected.");
            }//else
        }//for
    }//4

    if(query == 5){
        //Part 5 goes here
               System.out.print("Would you like to add or remove? ");
            String addremove = scan.next();
            if (addremove.equalsIgnoreCase("Add")){
                System.out.print("Enter your start location: ");
                city1 = scan.next();
                System.out.print("Enter your final destination: ");
                city2 = scan.next();
                System.out.print("Enter preferred distance: ");
                double distance = scan.nextDouble();
                System.out.print("Enter monetary values: ");
                double money = scan.nextDouble();
                int c = 0;
                int d = 0;
                for(int i = 0; i < D.V(); i++){
                   if(G.getCity(i).equals(city1)){
                        c = i;
                    }
                 }


    }//5

    else{
        System.exit(0);
    }//6
        public String getCity(int i){
        String city = cities[i];
        return city;}


    }}//main

}//lab8 class
