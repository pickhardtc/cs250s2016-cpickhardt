//Claire Pickhardt
//Professor Wenskovitch
//Computer Science 250
//4 March 2016
//Lab 5
//Honor Code: The work that I am submitting is the result of my own thinking and efforts.
//Any help I received, I received from either Professor Wenskovitch or SJ Guillaume, but then I asked Almog and he told me how to do the legal placement function in such a simple way that I literally slapped my forehead.
//Thanks Almog. You're the real MVP. Some of these lines play off your's.
import java.util.*;

public class EightQueensAdvanced{

        int row;
        int col;
        int sol;
        static boolean board[][];
        static int rownum;
        static int colnum;
        static int count = 0;
    public static void main(String args[]){
        Scanner scan = new Scanner(System.in);
        System.out.println("How many rows do you want?");
        rownum = scan.nextInt();
        System.out.println("How many columns do you want?");
        colnum = scan.nextInt();
        board = new boolean[rownum][colnum];
    }//main method

       public static void placeQueen(int row){
            for(int col = 0; col < colnum; col++){
                if(isLegalPlacement(row, col)){
                    addQueen(row, col);
                    if(row==rownum-1){
                        printsolution();
                    }//if
                    else{
                        placeQueen(row+1);
                    }//else
                board[row][col]=false;
                }//if
            }//nested for
    }//placeQueen

    public static boolean isLegalPlacement(int row, int col){
       int tempcol1=col;
       int tempcol2=col;
        if(row==0){
            return true;
        }//if
        else{
        for(int q = 7; q>= 0; q--){
            if(board[q][col]==true){
                return false;
            }//if
        if(tempcol1 > 0){
            if(board[q][--tempcol1]==true){
                return false;
            }//nested if <`0`>
        }//if
        if(tempcol2<rownum-1){
            if(board[q][++tempcol2]==true){
                return false;
                }//nested if
            }//if
        }//for
    }//else
            return true;
}//islegalplacement

    public static void addQueen(int col, int row){
        board[row][col] = true;
    }//addqueen




public static void printsolution(){
    for(int pos1=0; pos1<rownum; pos1++){
        for(int pos2=0; pos2<rownum; pos2++){
           if(board[pos1][pos2] == true){
            System.out.print("Q");}//if -> prints queens locations
           else{
               System.out.print(".");
           }//else -> prints where queens are not
        }//nested for
        System.out.print(" ");
    }//for
    System.exit(0);
    count++;
}//solution function

}//EightQueens class
