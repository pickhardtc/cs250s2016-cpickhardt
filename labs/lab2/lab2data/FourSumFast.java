//Claire Pickhardt
//Computer Science 250
//Lab 2
//5 February 2016
//Honor Code: The work that I am submitting is the result of my own thinking and efforts.

import java.io.*;
import java.util.*;

public class FourSumFast {

    public static void main(String args[]) {
  In in = new In(args[0]);
  int[] a = in.readAllInts();

  Stopwatch timer = new Stopwatch();
  StdOut.println("4-tuples: " + count(a));
  StdOut.println("Elapsed time: " + timer.elapsedTime());
    } //main

 public static int count(int[] a) {

  Arrays.sort(a);
  int count = 0;

  for (int i = 0; i < a.length; i++) {
   for (int j = i+1; j < a.length; j++) {
    for (int k = j +1; k < a.length; k++) {
    if ((BinarySearch.rank(-a[i]-a[j]-a[k], a) > k)) {
     count++;
     } //if
    } //for
   } //for
  } //for

  return count;

 } //count

} //FourSumFast (class)
